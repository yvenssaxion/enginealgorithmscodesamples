#include <stdio.h>
#include <stdlib.h>  
#include <cstring>
#include <chrono>
#include <ctime> 
#include <vector>

#pragma region SuperClass1

class A1;
class B1;
class C1;

class SuperClass1 {
    public: 
        int id = 0;
        int index = 0;
        char buffer[64];
    public:
        SuperClass1(void) : index(-1), id(0) {}
        SuperClass1(int index) : index(index), id(0) {}
    public:
        void printBuffer(void) {
            printf("[%d]: %s\n", index, buffer);
        }
        virtual void Act(SuperClass1* another);

        void ActWith(SuperClass1* another);
        void ActWith(A1* another);
        void ActWith(B1* another);
        void ActWith(C1* another);
};

class A1 : public SuperClass1 {
    public:
        A1(int index);
        virtual void ActWith(SuperClass1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] A1 Collide with another SuperClass1 %d\n", this->index, another->id);
        }

        virtual void ActWith(A1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] A1 Collide with another A1 %d\n", this->index, another->id);
        }

        virtual void ActWith(B1* another);

        virtual void ActWith(C1* another);
};
A1::A1(int index) : SuperClass1(index) {
    this->id = 1;
}

class B1 : public SuperClass1 {
    public:
        B1(int index);
        virtual void ActWith(SuperClass1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] B1 Collide with another SuperClass1 %d\n", this->index, another->id);
        }

        virtual void ActWith(A1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] B1 Collide with another A1 %d\n", this->index, another->id);
        }

        virtual void ActWith(B1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] B1 Collide with another B1 %d\n", this->index, another->id);
        }

        virtual void ActWith(C1* another);
};

B1::B1(int index) : SuperClass1(index) {
    this->id = 2;
}

class C1 : public SuperClass1 {
    public:
        C1(int index);
        virtual void ActWith(SuperClass1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] C1 Collide with another SuperClass1 %d\n", this->index, another->id);
        }

        virtual void ActWith(A1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] C1 Collide with another A1 %d\n", this->index, another->id);
        }

        virtual void ActWith(B1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] C1 Collide with another B1 %d\n", this->index, another->id);
        }

        virtual void ActWith(C1* another) {
            memset(this->buffer, 0, 64);
            sprintf(this->buffer, "[%d] C1 Collide with another C1 %d\n", this->index, another->id);
        }
};

C1::C1(int index) : SuperClass1(index) {
    this->id = 3;
}

void A1::ActWith(B1* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] A1 Collide with another B1 %d\n", this->index, another->id);
}

void A1::ActWith(C1* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] A1 Collide with another C1 %d\n", this->index, another->id);
}

void B1::ActWith(C1* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] B1 Collide with another C1 %d\n", this->index, another->id);
}

void SuperClass1::Act(SuperClass1* another) {
    switch(this->id){
        case 0: 
            switch(another->id) {
                case 0: 
                    this->ActWith(another);
                break;
                case 1:
                    this->ActWith(static_cast<A1*>(another));
                break;
                case 2:
                    this->ActWith(static_cast<B1*>(another));
                break;
                case 3:
                    this->ActWith(static_cast<C1*>(another));
                break;
            }
        break;

        case 1:
            switch(another->id) {
                case 0: 
                    (static_cast<A1*>(this))->ActWith(another);
                break;
                case 1:
                    (static_cast<A1*>(this))->ActWith(static_cast<A1*>(another));
                break;
                case 2:
                    (static_cast<A1*>(this))->ActWith(static_cast<B1*>(another));
                break;
                case 3:
                    (static_cast<A1*>(this))->ActWith(static_cast<C1*>(another));
                break;
            }
        break;

        case 2:
            switch(another->id) {
                case 0: 
                    (static_cast<B1*>(this))->ActWith(another);
                break;
                case 1:
                    (static_cast<B1*>(this))->ActWith(static_cast<A1*>(another));
                break;
                case 2:
                    (static_cast<B1*>(this))->ActWith(static_cast<B1*>(another));
                break;
                case 3:
                    (static_cast<B1*>(this))->ActWith(static_cast<C1*>(another));
                break;
            }
        break;

        case 3:
            switch(another->id) {
                case 0: 
                    (static_cast<C1*>(this))->ActWith(another);
                break;
                case 1:
                    (static_cast<C1*>(this))->ActWith(static_cast<A1*>(another));
                break;
                case 2:
                    (static_cast<C1*>(this))->ActWith(static_cast<B1*>(another));
                break;
                case 3:
                    (static_cast<C1*>(this))->ActWith(static_cast<C1*>(another));
                break;
            }
        break;
    }
    
}

void SuperClass1::ActWith(SuperClass1* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass1 with another SuperClass1 %d\n", this->index, another->id);
}

void SuperClass1::ActWith(A1* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass1 with another A1 %d\n", this->index, another->id);
}

void SuperClass1::ActWith(B1* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass1 with another B1 %d\n", this->index, another->id);
}

void SuperClass1::ActWith(C1* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass1 with another C1 %d\n", this->index, another->id);
}

#pragma endregion

#pragma region SuperClass2
class A2;
class B2;
class C2;

class SuperClass2 {
    public: 
        int id = 0;
        int index = 0;
        char buffer[64];
    public:
        SuperClass2(void) : index(-1), id(0) {}
        SuperClass2(int index) : index(index), id(0) {}

        void printBuffer(void) {
            printf("[%d]: %s\n", index, buffer);
        }
    public:
        virtual void ActWith(SuperClass2* another);
        virtual void ActWith(A2* another);
        virtual void ActWith(B2* another);
        virtual void ActWith(C2* another);
        virtual void isActedWith(SuperClass2* another);
        virtual void isActedWith(A2* another);
        virtual void isActedWith(B2* another);
        virtual void isActedWith(C2* another);
};

class A2 : public SuperClass2 {
    public:
        A2(int index);
    public:
        virtual void ActWith(SuperClass2* another) override;
        virtual void ActWith(A2* another) override;
        virtual void ActWith(B2* another) override;
        virtual void ActWith(C2* another) override;
        virtual void isActedWith(SuperClass2* another) override;
        virtual void isActedWith(A2* another) override;
        virtual void isActedWith(B2* another) override;
        virtual void isActedWith(C2* another) override;
};
A2::A2(int index) : SuperClass2(index) {
    this->id = 1;
}

class B2 : public SuperClass2 {
    public:
        B2(int index);
    public:
        virtual void ActWith(SuperClass2* another) override;
        virtual void ActWith(A2* another) override;
        virtual void ActWith(B2* another) override;
        virtual void ActWith(C2* another) override;
        virtual void isActedWith(SuperClass2* another) override;
        virtual void isActedWith(A2* another) override;
        virtual void isActedWith(B2* another) override;
        virtual void isActedWith(C2* another) override;
};
B2::B2(int index) : SuperClass2(index) {
    this->id = 2;
}

class C2 : public SuperClass2 {
    public:
        C2(int index);
    public:
        virtual void ActWith(SuperClass2* another) override;
        virtual void ActWith(A2* another) override;
        virtual void ActWith(B2* another) override;
        virtual void ActWith(C2* another) override;
        virtual void isActedWith(SuperClass2* another) override;
        virtual void isActedWith(A2* another) override;
        virtual void isActedWith(B2* another) override;
        virtual void isActedWith(C2* another) override;
};
C2::C2(int index) : SuperClass2(index) {
    this->id = 3;
}

//SuperClass2
void SuperClass2::ActWith(SuperClass2* another) {
    (static_cast<SuperClass2*>(another))->isActedWith(static_cast<SuperClass2*>(this));
}
void SuperClass2::ActWith(A2* another) {
    (static_cast<A2*>(another))->isActedWith(static_cast<SuperClass2*>(this));
}
void SuperClass2::ActWith(B2* another) {
    (static_cast<B2*>(another))->isActedWith(static_cast<SuperClass2*>(this));
}
void SuperClass2::ActWith(C2* another) {
    (static_cast<C2*>(another))->isActedWith(static_cast<SuperClass2*>(this));
}

void SuperClass2::isActedWith(SuperClass2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass2 with another SuperClass2 %d\n", another->index, this->id);
}
void SuperClass2::isActedWith(A2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass2 with another A2 %d\n", another->index, this->id);
}
void SuperClass2::isActedWith(B2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass2 with another B2 %d\n", another->index, this->id);
}
void SuperClass2::isActedWith(C2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] SuperClass2 with another C2 %d\n", another->index, this->id);
}

//A2
void A2::ActWith(SuperClass2* another) {
    (static_cast<SuperClass2*>(another))->isActedWith(static_cast<A2*>(this));
}
void A2::ActWith(A2* another) {
    (static_cast<A2*>(another))->isActedWith(static_cast<A2*>(this));
}
void A2::ActWith(B2* another) {
    (static_cast<B2*>(another))->isActedWith(static_cast<A2*>(this));
}
void A2::ActWith(C2* another) {
    (static_cast<C2*>(another))->isActedWith(static_cast<A2*>(this));
}

void A2::isActedWith(SuperClass2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] A2 with another SuperClass2 %d\n", another->index, this->id);
}
void A2::isActedWith(A2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] A2 with another A2 %d\n", another->index, this->id);
}
void A2::isActedWith(B2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] A2 with another B2 %d\n", another->index, this->id);
}
void A2::isActedWith(C2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] A2 with another C2 %d\n", another->index, this->id);
}

//B2
void B2::ActWith(SuperClass2* another) {
    (static_cast<SuperClass2*>(another))->isActedWith(static_cast<B2*>(this));
}
void B2::ActWith(A2* another) {
    (static_cast<A2*>(another))->isActedWith(static_cast<B2*>(this));
}
void B2::ActWith(B2* another) {
    (static_cast<B2*>(another))->isActedWith(static_cast<B2*>(this));
}
void B2::ActWith(C2* another) {
    (static_cast<C2*>(another))->isActedWith(static_cast<B2*>(this));
}

void B2::isActedWith(SuperClass2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] B2 with another SuperClass2 %d\n", another->index, this->id);
}
void B2::isActedWith(A2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] B2 with another A2 %d\n", another->index, this->id);
}
void B2::isActedWith(B2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] B2 with another B2 %d\n", another->index, this->id);
}
void B2::isActedWith(C2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] B2 with another C2 %d\n", another->index, this->id);
}

//C2
void C2::ActWith(SuperClass2* another) {
    (static_cast<SuperClass2*>(another))->isActedWith(static_cast<C2*>(this));
}
void C2::ActWith(A2* another) {
    (static_cast<A2*>(another))->isActedWith(static_cast<C2*>(this));
}
void C2::ActWith(B2* another) {
    (static_cast<B2*>(another))->isActedWith(static_cast<C2*>(this));
}
void C2::ActWith(C2* another) {
    (static_cast<C2*>(another))->isActedWith(static_cast<C2*>(this));
}

void C2::isActedWith(SuperClass2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] C2 with another SuperClass2 %d\n", another->index, this->id);
}
void C2::isActedWith(A2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] C2 with another A2 %d\n", another->index, this->id);
}
void C2::isActedWith(B2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] C2 with another B2 %d\n", another->index, this->id);
}
void C2::isActedWith(C2* another) {
    memset(this->buffer, 0, 64);
    sprintf(this->buffer, "[%d] C2 with another C2 %d\n", another->index, this->id);
}
#pragma endregion

int main(int argc, char *argv[])
{
    const int size = atoi(argv[1]);
    const int numberOfTests = atoi(argv[2]);
    
    std::vector<SuperClass1*> listOfObjects1(size);
    std::vector<SuperClass2*> listOfObjects2(size);
    srand(6);

    auto initial = std::chrono::system_clock::now();
    int random = -1;
    int distribution[4];
    memset(distribution, 0, 4);
	for (int i = 0; i < size; i++)
	{
        random = rand() % 41;
        if(random < 10) {
            distribution[0]++;
            listOfObjects1[i] = new SuperClass1(i);
            listOfObjects2[i] = new SuperClass2(i);
        } else if(random < 20) {
            distribution[1]++;
            listOfObjects1[i] = new A1(i);
            listOfObjects2[i] = new A2(i);
        } else if(random < 30) {
            distribution[2]++;
            listOfObjects1[i] = new B1(i);
            listOfObjects2[i] = new B2(i);
        } else {
            distribution[3]++;
            listOfObjects1[i] = new C1(i);
            listOfObjects2[i] = new C2(i);
        }
	}

    auto afterSetup = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = afterSetup - initial;
    printf("Elapsed time for initialization;%f\n", elapsed_seconds.count());

    printf("Distribution;%3d;%3d;%3d;%3d\n", distribution[0], distribution[1], distribution[2], distribution[3]);
    
    printf("Test;Single Dispatch;Double Dispatch\n");

    // #define DEBUG
    for (int i = 0; i < numberOfTests; i++)
	{
        printf("%d;", (i+1));
        initial = std::chrono::system_clock::now();
        for(SuperClass1* obj1 : listOfObjects1) {
            for(SuperClass1* obj2 : listOfObjects1) {
                obj1->Act(obj2);
                #ifdef DEBUG
                    obj1->printBuffer();
                #endif
            }    
        }
        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

        initial = std::chrono::system_clock::now();
        for(SuperClass2* obj1 : listOfObjects2) {
            for(SuperClass2* obj2 : listOfObjects2) {
                obj1->ActWith(obj2);
                #ifdef DEBUG
                    obj2->printBuffer();
                #endif
            }    
        }
        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f\n", elapsed_seconds.count());
    }

	return 0;
}