Test setup;2000;2
Setup array with objects;2000
Elapsed time for initialization;0.020301
Exp1 (Simple i-j);Exp2 (Swapped j-i);Exp3 (Aditional local variables);Exp4 (4 per time in J);Exp5 (8 per time in J);Exp6 (4 per time swapped J-I);Exp7 (8 per time swapped J-I);Exp8 (Linear Memory)
   1;0.013089;0.056306;0.060120;0.012576;0.012555;0.018813;0.015206;0.013936;
   2;0.012721;0.056904;0.058566;0.013399;0.012548;0.020953;0.014600;0.012971;
Test setup;8000;2
Setup array with objects;8000
Elapsed time for initialization;0.306391
Exp1 (Simple i-j);Exp2 (Swapped j-i);Exp3 (Aditional local variables);Exp4 (4 per time in J);Exp5 (8 per time in J);Exp6 (4 per time swapped J-I);Exp7 (8 per time swapped J-I);Exp8 (Linear Memory)
   1;0.211915;1.179599;1.123085;0.205294;0.230867;0.335472;0.230248;0.205906;
   2;0.205976;1.232386;1.220137;0.202129;0.208181;0.313175;0.213846;0.202781;
