#include <stdio.h>
#include <stdlib.h>  
#include <cstring>
#include <chrono>
#include <ctime> 

int main(int argc, char *argv[])
{
    const int size = atoi(argv[1]);
    const int numberOfTests = atoi(argv[2]);
    printf("Test setup;%d;%d\n", size, numberOfTests);
    float** myArray = new float*[size];
    float* myLinearArray = new float[size*size];
    std::memset(myArray, 0, size * sizeof(float));
    std::memset(myLinearArray, 0, size * size * sizeof(float));
    for (int i = 0; i < size; i++)
	{
        myArray[i] = new float[size];
        std::memset(myArray[i], 0, size * sizeof(float));
    }

    float total = 0;
    printf("Setup array with objects;%d\n", size);
    auto initial = std::chrono::system_clock::now();
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			myArray[i][j] = i * size + j;
            myLinearArray[j + i*size] = i * size + j;   
		}
	}
    auto afterSetup = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = afterSetup - initial;
    printf("Elapsed time for initialization;%f\n", elapsed_seconds.count());
    
    printf("Exp1 (Simple i-j);Exp2 (Swapped j-i);Exp3 (Aditional local variables);Exp4 (4 per time in J);Exp5 (8 per time in J);Exp6 (4 per time swapped J-I);Exp7 (8 per time swapped J-I);Exp8 (Linear Memory)\n");
	for (int i = 1; i <= numberOfTests; i++) {
        printf("%4d;", i);
        initial = std::chrono::system_clock::now();

        total = 0;
        for (int ii = 0; ii < size; ii++)
        {
            for (int jj = 0; jj < size; jj++)
            {
                total += myArray[ii][jj];
            }
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());
        
        initial = std::chrono::system_clock::now();
        total = 0;
        for (int ii = 0; ii < size; ii++)
        {
            for (int jj = 0; jj < size; jj++)
            {
                total += myArray[jj][ii];
            }
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

        initial = std::chrono::system_clock::now();
        total = 0;
        for (int ii = 0; ii < size; ii++)
        {
            double subTotal = 0;
            for (int jj = 0; jj < size; jj++)
            {
                double localValue = myArray[jj][ii];
                subTotal += localValue;
            }
            total += subTotal;
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

        initial = std::chrono::system_clock::now();
        total = 0;
        for (int ii = 0; ii < size; ii++)
        {
            for (int jj = 0; jj < size; jj += 4)
            {
                total += myArray[ii][jj + 0];
                total += myArray[ii][jj + 1];
                total += myArray[ii][jj + 2];
                total += myArray[ii][jj + 3];
            }
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

        initial = std::chrono::system_clock::now();
        total = 0;
        for (int ii = 0; ii < size; ii++)
        {
            for (int jj = 0; jj < size; jj += 8)
            {
                total += myArray[ii][jj + 0];
                total += myArray[ii][jj + 1];
                total += myArray[ii][jj + 2];
                total += myArray[ii][jj + 3];
                total += myArray[ii][jj + 4];
                total += myArray[ii][jj + 5];
                total += myArray[ii][jj + 6];
                total += myArray[ii][jj + 7];
            }
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

        initial = std::chrono::system_clock::now();
        total = 0;
        for (int ii = 0; ii < size; ii++)
        {
            for (int jj = 0; jj < size; jj += 4)
            {
                total += myArray[jj][ii + 0];
                total += myArray[jj][ii + 1];
                total += myArray[jj][ii + 2];
                total += myArray[jj][ii + 3];
            }
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

        initial = std::chrono::system_clock::now();
        total = 0;
        for (int ii = 0; ii < size; ii++)
        {
            for (int jj = 0; jj < size; jj += 8)
            {
                total += myArray[jj][ii + 0];
                total += myArray[jj][ii + 1];
                total += myArray[jj][ii + 2];
                total += myArray[jj][ii + 3];
                total += myArray[jj][ii + 4];
                total += myArray[jj][ii + 5];
                total += myArray[jj][ii + 6];
                total += myArray[jj][ii + 7];
            }
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

    // IGNORE FIRST!
    #pragma region linear
        initial = std::chrono::system_clock::now();
        total = 0;
        for (int ii = 0; ii < size*size; ii++)
        {
            total += myLinearArray[ii];
        }

        afterSetup = std::chrono::system_clock::now();
        elapsed_seconds = afterSetup - initial;
        printf("%6.6f;", elapsed_seconds.count());

        printf("\n");
    #pragma endregion
    }

	return 0;
}